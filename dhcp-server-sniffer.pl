#!/usr/bin/perl

#     Copyright (C) 2015 Anthony Walters <anto.walters@gmail.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

# CPAN libraries need to be available on your system:
#    http://search.cpan.org/dist/Net-Pcap/
#    http://search.cpan.org/dist/NetPacket/
#    http://search.cpan.org/dist/Net-DHCP/
# To install these on a debian system:
#    apt-get install libnetpacket-perl libnet-pcap-perl libnet-dhcp-perl
#
# Notes for constructing a Pcap filter, the filter is already set up, this link is just here for reference.
#    http://www.wains.be/pub/networking/tcpdump_advanced_filters.txt

#libpcap for sniffing the network
use Net::Pcap;

#libs for stripping the outer protocols
use NetPacket::Ethernet qw(:types);
use NetPacket::IP;
use NetPacket::UDP;

#libs for decoding the DHCP payload
use Net::DHCP::Packet;
use Net::DHCP::Constants;

#Set to 1 for verbose output, 0 for non verbose output
my $verbose_output=0;

my %dhcp_message_type_string = (
   "1" => "DHCP Discover",
   "2" => "DHCP Offer",
   "3" => "DHCP Request",
   "4" => "DHCP Decline",
   "5" => "DHCP Ack",
   "6" => "DHCP Nak",
   "7" => "DHCP Release",
   "8" => "DHCP Inform",
   );

my $err;

# use a command line passed device, or look one up.
my $physical_device = $ARGV[0];
unless (defined $physical_device) {
   $physical_device = Net::Pcap::lookupdev(\$err);
   if (defined $err) {
      die 'Unable to determine network device for monitoring - ', $err;
   }
}

# check for an ip address and network mask or else an error is returned.
# This method is useful for the validation of a device name supplied for network monitoring by a user.
my ($address, $netmask);
if (Net::Pcap::lookupnet($physical_device, \$address, \$netmask, \$err)) {
   die 'Unable to look up device information for ', $physical_device, ' - ', $err;
}

# Create packet capture object on device
my $capture_object;
my $num_bytes='1500';      #Maximum number of bytes to capture from each packet
my $promisc=1;             #1 is promiscious, 0 is non promiscious
my $to_ms=0;               #A $to_ms value of 0 captures packets until an error occurs while a value of -1 captures packets indefinitely.
$capture_object = Net::Pcap::open_live($physical_device, $num_bytes, $promisc, $to_ms, \$err);
unless (defined $capture_object) {
   die 'Unable to create packet capture on device ', $physical_device, ' - ', $err;
}

# The current filter only filters for DHCP ethernet broadcasts
#   '((port 67 or port 68) and (ether dst host ff:ff:ff:ff:ff:ff))'
my $filter;
Net::Pcap::compile(
   $capture_object,
   \$filter,
   '((port 67 or port 68) and (ether dst host ff:ff:ff:ff:ff:ff))',
   0,
   $netmask
) && die 'Unable to compile packet capture filter';
Net::Pcap::setfilter($capture_object, $filter) &&
   die 'Unable to set packet capture filter';

#print some header info
printf "%-17s | %-13s | %-15s | %-12s\n", "Timestamp", "Message Type", "DHCP Server IP", "Ether MAC";

# Set callback function and initiate packet capture loop
my $packet_count='-1';      #The number of packets to capture, -1 means infinate
Net::Pcap::loop($capture_object, $packet_count, \&process_packet, '') ||
   die 'Unable to perform packet capture';

Net::Pcap::close($capture_object);

# The function that gets called by pcap
sub process_packet {
   # $user_data is empty, $header is not used, $packet contains the raw ethernet packet from the network
   my ($user_data, $header, $packet) = @_;
   my $timestamp = getLoggingTime();

   #first upwrap the protocols
   my $eth_obj = NetPacket::Ethernet->decode($packet);
   my $ip_obj = NetPacket::IP->decode($eth_obj->{data});
   my $udp_obj = NetPacket::UDP->decode($ip_obj->{data});

   # the data part of the udp packet contains the DHCP protocol 
   # Parse the UDP bootp/dhcp payload with Net::DHCP to get at the DHCP information
   my $dhcp_obj = Net::DHCP::Packet->new($udp_obj->{data});

   # we want to look at packets that contain a DHCP server identifier
   # http://search.cpan.org/dist/Net-DHCP/lib/Net/DHCP/Packet.pm
   if (my $dhcp_server_ipaddress = $dhcp_obj->getOptionValue(DHO_DHCP_SERVER_IDENTIFIER())) {
   
      # print the message type and the ipaddress of the DHCP server
      printf "%-17s | %-13s | %-15s", 
         $timestamp,
         $dhcp_message_type_string{$dhcp_obj->getOptionValue(DHO_DHCP_MESSAGE_TYPE())},
         $dhcp_server_ipaddress;

      #Now to print a mac address, but this only makes sense when the server is replying
      #look at page 10 here http://tools.ietf.org/html/rfc2131#page-10
      if ($dhcp_obj->op() == BOOTREPLY()) {
         printf " | %-12s", $eth_obj->{src_mac};
      }

      #now end all that output with a newline
      print "\n";
      
      if ($verbose_output) {
         print "eth: src_mac $eth_obj->{src_mac} dest_mac $eth_obj->{dest_mac} type $eth_obj->{type}\n";
         print "ip : ver $ip_obj->{ver} hlen $ip_obj->{hlen} flags $ip_obj->{flags} foffset $ip_obj->{foffset} tos $ip_obj->{tos} len $ip_obj->{len} ",
               "id $ip_obj->{id} ttl $ip_obj->{ttl} proto $ip_obj->{proto} cksum $ip_obj->{cksum} ",
               "src_ip $ip_obj->{src_ip} dest_ip $ip_obj->{dest_ip} options $ip_obj->{options}\n";
         print "udp: src_port $udp_obj->{src_port} dest_port $udp_obj->{dest_port} len $udp_obj->{len} cksum $udp_obj->{cksum}\n";

         print "-----------------------------------------------------------\n";
         print $dhcp_obj->toString();
         print "-----------------------------------------------------------\n";
         print "\n";
      }
   }
}

sub getLoggingTime {

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
    my $nice_timestamp = sprintf ( "%04d%02d%02d %02d:%02d:%02d",
                                   $year+1900,$mon+1,$mday,$hour,$min,$sec);
    return $nice_timestamp;
}
